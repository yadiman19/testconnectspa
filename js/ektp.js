let port = null
const opens = async () => {
    // Promp user to select any serial port
    port = await navigator.serial.requestPort();
    await port.open({ baudRate: 115200 });
    console.log(port)

    // Listen to serial data
    const reader = port.readable.getReader();

    let tmp = []
    let dataLenh = 0
    let dataLenl = 0
    let data = []

    while (true) {
        const { value, done } = await reader.read();

        if (done) {
            reader.releaseLock();
            break;
        }
        for (let i = 0; i < value.length; i++) {
            tmp.push(value[i])
            if (tmp.length == 4) {
                dataLenh = tmp[2]
                dataLenl = tmp[3]
            }

            if (tmp.length >= 5 && (data.length <= (dataLenh * 255) + dataLenl)) {
                data.push(value[i]) 
            };

            if (tmp.length == (4 + (dataLenh * 255) + dataLenl + 3)) {
                let sw1 = decimalToHexString(tmp[tmp.length -3]);
                let sw2 = decimalToHexString(tmp[tmp.length -2]);
                
                data.pop()
                prosesData(tmp, data, sw1, sw2);
                tmp = []
                data = []
            }
        }
    }
}

const prosesData = (commandData, data, sw1, sw2) => {
    // buat validasi atas global response dari sw1 dan sw2
    let newFormat = []
    if (commandData[1] == 185) {                                        // 185 is 0xB9
        for (let i = 0; i < commandData.length; i++) {
            newFormat.push(decimalToHexString(commandData[i]))
        }
        // console.log(newFormat)
        alert('Ready to use')
        console.log("Home screen selesai")
    } else if (commandData[1] == 181 && commandData.length <= 7) {      // 181 is 0xB5 // ganti datalent
        for (let i = 0; i < commandData.length; i++) {
            newFormat.push(decimalToHexString(commandData[i]))
        }
        // console.log(newFormat)
        alert('No Card Detected')

        let h = document.getElementById('ktp').innerHTML = "<p>KTP tidak ditemukan</p>"

    } else if (commandData[1] == 181 && commandData.length > 7) {       // 181 is 0xB5
        for (let i = 0; i < commandData.length; i++) {
            newFormat.push(decimalToHexString(commandData[i]))
        }
        // console.log(newFormat)
        let h = document.getElementById('ktp').innerHTML = ""

        let kodeJari = newFormat[4]
        let kodeJariCadangan = newFormat[5]



        bacaJari()
        if (kodeJari == '1') {
            alert("Letakkan Ibu Jari Kanan Anda ")
        } else if (kodeJari == '2') {
            alert("Letakkan Jari Telunjuk Kanan Anda ")
        } else if (kodeJari == '3') {
            alert("Letakkan Jari Tengah Kanan Anda ")
        } else if (kodeJari == '4') {
            alert("Letakkan Jari Manis Kanan Anda ")
        } else if (kodeJari == '5') {
            message = controllerLanguage.isEnglish
            alert("Letakkan Jari Kelingking Kanan Anda ")
        } else if (kodeJari == '6') {
            alert("Letakkan Ibu Jari Kiri Anda ")
        } else if (kodeJari == '7') {
            message = controllerLanguage.isEnglish
            alert("Letakkan Jari Telunjuk Kiri Anda ")
        } else if (kodeJari == '8') {
            message = controllerLanguage.isEnglish
            alert("Letakkan Jari Tengah Kiri Anda ")
        } else if (kodeJari == '9') {
            message = controllerLanguage.isEnglish
            alert("Letakkan Jari Manis Kiri Anda ")
        } else if (kodeJari == '0A') {
            message = controllerLanguage.isEnglish
            alert("Letakkan Jari Kelingking Kiri Anda ")
        } else {
            alert("Ooops Something Wrong")
        }


        console.log("Baca KTP Selesai")
    } else if (commandData[1] == 182) {                                 // 182 is 0XB6
        // no need response
        console.log("jari selesai")
    } else if (commandData[1] == 183 && commandData.length > 7) {       // 183 is 0XB7
        for (let i = 0; i < commandData.length; i++) {
            newFormat.push(decimalToHexString(commandData[i]))
        }
        // console.log(newFormat)


        for (let j = 0; j < newFormat.length; j++) {
            if (newFormat[j] == '90' && newFormat[j + 1] == 0) {
                sendBioData()
                alert('Match jari selesai')
            } else if (newFormat[j] == '6F' && newFormat[j + 1] == 0) {
                alert('Match Jari gagal')
            } else if (newFormat[j] == '6A' && newFormat[j + 1] == 92) {
                alert('MatcH jari gagal , terlalu banyak percobaan')
            }
        }

        // alert('Baca Jari gagal')
        console.log("Match jari selesai")
    } else if (commandData[1] == 177) {
        let biodata = {
            "nik":"",
            "nama":"",
            "alamat":"",
            "rt":"",
            "rw":"",
            "tempatLahir":"",
            "tanggalLahir":"",
            "kecamatan":"",
            "kelurahan":"",
            "kabupaten":"",
            "provinsi":"",
            "jenisKelamin":"",
            "agama":"",
            "statusPerkawinan":"",
            "pekerjaan":"",
            "kewarganegaraan":"",
            "berlakuHingga":"",
            "rawData": "",
        }
        let str = ""
        for (let i = 0; i < data.length; i++) {
            if (String.fromCharCode(data[i]) !== '"') {
                str += String.fromCharCode(data[i])
                newFormat.push(decimalToHexString(commandData[i]))

            }
        }

        let newStr = str.split(",")
        console.log(newStr)
        biodata.nik = newStr[0]
        biodata.nama = newStr[13]
        biodata.alamat = newStr[1]
        biodata.rt = newStr[2]
        biodata.rw = newStr[3]
        biodata.tempatLahir = newStr[4]
        biodata.tanggalLahir = newStr[14]
        biodata.kecamatan = newStr[5]
        biodata.kelurahan = newStr[6]
        biodata.kabupaten = newStr[7]
        biodata.provinsi = newStr[15]
        biodata.jenisKelamin = newStr[8]
        biodata.agama = newStr[10]
        biodata.statusPerkawinan = newStr[11]
        biodata.pekerjaan = newStr[12]
        biodata.kewarganegaraan = newStr[19]
        biodata.berlakuHingga = newStr[16]


        let h = document.getElementById('ktp').innerHTML = `
        <ul>
            <li>${biodata.nik}</li>
            <li>${biodata.nama}</li>
            <li>${biodata.alamat}</li>
            <li>${biodata.rt}</li>
            <li>${biodata.rw}</li>
            <li>${biodata.tempatLahir}</li>
            <li>${biodata.tanggalLahir}</li>
            <li>${biodata.kecamatan}</li>
            <li>${biodata.kelurahan}</li>
            <li>${biodata.kabupaten}</li>
            <li>${biodata.provinsi}</li>
            <li>${biodata.jenisKelamin}</li>
            <li>${biodata.agama}</li>
            <li>${biodata.statusPerkawinan}</li>
            <li>${biodata.pekerjaan}</li>
            <li>${biodata.kewarganegaraan}</li>
            <li>${biodata.berlakuHingga}</li>
        </ul/>
        `
    } else if (commandData[1] == 162) {
        console.log(commandData,'command Data')
        console.log(data,'data')
    } else if (commandData[1] == 163) {
        console.log(commandData,'command Data')
        console.log(data,'data')
    }

    data = []
}

// Real Command ===================================================================================================

const getLRC = (bytes, start, end) => {
    let LRC = 0;
    for (let i = start; i <= end; i++) {
        LRC = (bytes[i] ^ LRC);
    }

    return LRC;
}


const EKTPCommand_Homescreen = () => {
    const command = new Uint8Array(5);
    command[0] = 0x3F; //stx
    command[1] = 0xB9;
    command[2] = 0x00; //LenH
    command[3] = 0x00; //LenL
    command[4] = getLRC(command, 1, 3 + (command[2] * 256 + command[3]));

    return command;
}

const EKTPCommand_Reboot = () => {
    const command = new Uint8Array(6);
    command[0] = 0x3F; //stx
    command[1] = 0x95; //INS //B9 B5 B6 B7
    command[2] = 0x00; //LenH
    command[3] = 0x01; //LenL
    command[4] = 0x01; //SW1 <--- DATA1
    command[5] = getLRC(command, 1, 3 + (command[2] * 256 + command[3]));

    return command;
}

const EKTPCommand_Read = () => {
    const command = new Uint8Array(5);
    command[0] = 0x3F; //stx
    command[1] = 0xB5; //INS //B9 B5 B6 B7
    command[2] = 0x00; //LenH
    command[3] = 0x00; //LenL
    command[4] = getLRC(command, 1, 3 + (command[2] * 256 + command[3]));

    let h = document.getElementById('ktp').innerHTML = `
    <div><h1 style="text-align:center;">LOADING....</h1></div>
    `

    return command;
}

const EKTPCommand_Finger = () => {
    const command = new Uint8Array(7);
    command[0] = 0x3F; //stx
    command[1] = 0xB6; //INS //B9 B5 B6 B7
    command[2] = 0x00; //LenH
    command[3] = 0x02; //LenL
    command[4] = 0X02; //SW1 <---- SALAAH HARUSNYA DATA1
    command[5] = 0x00; //SW2 <---- DATA 2
    command[6] = getLRC(command, 1, 3 + (command[2] * 256 + command[3]));
    return command;
}

const EKTPCommand_MatchFinger = (finger) => {
    const command = new Uint8Array(7);
    command[0] = 0x3F; //stx
    command[1] = 0xB7; //INS //B9 B5 B6 B7
    command[2] = 0x00; //LenH
    command[3] = 0x02; //LenL
    command[4] = 0x02; //SW1 <--- DATA1
    command[5] = 0x01; //SW2 <--- DATA2
    command[6] = getLRC(command, 1, 3 + (command[2] * 256 + command[3]));
    return command;
}

const EKTPCommand_BioData = () => {
    const command = new Uint8Array(5);
    command[0] = 0x3F; //stx
    command[1] = 0xB1; //INS //B9 B5 B6 B7
    command[2] = 0x00; //LenH
    command[3] = 0x00; //LenL
    command[4] = getLRC(command, 1, 3 + (command[2] * 256 + command[3]));

    return command;
}

const EKTPCommand_GetDataPhoto = () => {
    const command = new Uint8Array(5);
    command[0] = 0x3F; //stx
    command[1] = 0xA2; //INS //B9 B5 B6 B7
    command[2] = 0x00; //LenH
    command[3] = 0x00; //LenL
    command[4] = getLRC(command, 1, 3 + (command[2] * 256 + command[3]));

    return command;
}

const EKTPCommand_GetPhoto = () => {
    const command = new Uint8Array(5);
    command[0] = 0x3F; //stx
    command[1] = 0xA3; //INS //B9 B5 B6 B7
    command[2] = 0x00; //LenH
    command[3] = 0x00; //LenL
    command[4] = getLRC(command, 1, 3 + (command[2] * 256 + command[3]));

    return command;
}


const hexToByte = (hex) => {
    const key = '0123456789abcdef'
    let newBytes = []
    let currentChar = 0
    let currentByte = 0
    for (let i = 0; i < hex.length; i++) {   // Go over two 4-bit hex chars to convert into one 8-bit byte
        currentChar = key.indexOf(hex[i])
        if (i % 2 === 0) { // First hex char
            currentByte = (currentChar << 4) // Get 4-bits from first hex char
        }
        if (i % 2 === 1) { // Second hex char
            currentByte += (currentChar)     // Concat 4-bits from second hex char
            newBytes.push(currentByte)       // Add byte
            // input  => hexToByte('68656c6c6f')
            // output => [104, 101, 108, 108, 111]
        }
    }
    console.log(newBytes, 'hex to byte')
    return new Uint8Array(newBytes)
}

const decoder = new TextDecoder('UTF-8');

const toString = (data) => {
    const extraByteMap = [1, 1, 1, 1, 2, 2, 3, 0];
    var count = data.length;
    var str = "";

    for (var index = 0; index < count;) {
        var ch = data[index++];
        if (ch & 0x80) {
            var extra = extraByteMap[(ch >> 3) & 0x07];
            if (!(ch & 0x40) || !extra || ((index + extra) > count))
                return null;

            ch = ch & (0x3F >> extra);
            for (; extra > 0; extra -= 1) {
                var chx = data[index++];
                if ((chx & 0xC0) != 0x80)
                    return null;

                ch = (ch << 6) | (chx & 0x3F);
            }
        }

        str += String.fromCharCode(ch);
    }

    return str;
};


function convert(int) {
    const code = 'A'.charCodeAt(0);
    console.log(code); // 👉️ 65

    return String.fromCharCode(code + int);
}

// Main Function ===================================================================================================
const homescreen = async () => {
    const writer = port.writable.getWriter();
    await writer.write(EKTPCommand_Homescreen());

    writer.releaseLock();
}

const bacaKtp = async () => {
    const writer = port.writable.getWriter();
    await writer.write(EKTPCommand_Read());

    writer.releaseLock();
}

const sendBioData = async () => {
    // _biodatas.clear();
    const writer = port.writable.getWriter();
    await writer.write(EKTPCommand_BioData());

    writer.releaseLock();
}

const requestPhoto = async () => {
    const writer = port.writable.getWriter();
    await writer.write(EKTPCommand_GetDataPhoto());

    writer.releaseLock();
}

const getPhoto = async () => {
    const writer = port.writable.getWriter();
    await writer.write(EKTPCommand_GetPhoto());

    writer.releaseLock();
}

const resetAlat = async () => {
    const writer = port.writable.getWriter();
    await writer.write(EKTPCommand_Reboot());

    writer.releaseLock();
}

const bacaJari = async (finger) => {
    const writer = port.writable.getWriter();
    await writer.write(EKTPCommand_Finger());
    writer.releaseLock();
}

const matchJari = async (finger) => {
    const writer = port.writable.getWriter();
    await writer.write(EKTPCommand_MatchFinger());

    writer.releaseLock();
}


function decimalToHexString(number) {
    if (number < 0) {
        number = 0xFFFFFFFF + number + 1;
    }

    return number.toString(16).toUpperCase();
}

let gotoanotherpage = () => {
    cobaini()
}

let cobaini = (event) => {
    event = event || window.event;
    event.preventDefault();
    window.history.pushState({}, "", 'http://127.0.0.1:5500/about');
    handleLocation();
}